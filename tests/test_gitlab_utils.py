import unittest
from pytest import mark
from gitlab_ps_utils import gitlab_utils

@mark.unit_test
class GitLabUtilsTests(unittest.TestCase):
    def test_sanitize_name_project(self):
        with self.assertLogs(gitlab_utils.log, level="WARNING"):
            assert gitlab_utils.sanitize_name(
                " !  _-:: This.is-how/WE do\n&it#? - šđžčć_  ? ", "full_path") == "This.is-how WE do it - šđžčć"

    def test_sanitize_name_group(self):
        with self.assertLogs(gitlab_utils.log, level="ERROR"):
            assert gitlab_utils.sanitize_name(" !  _-:: This.is-how/WE do\n&it#? - (šđžčć)_  ? ",
                                        "full_path", is_group=True) == "This.is-how WE do it - (šđžčć"

    def test_sanitize_project_path(self):
        with self.assertLogs(gitlab_utils.log, level="WARNING"):
            assert gitlab_utils.sanitize_project_path(
                "!_-::This.is;;-how_we--do\n&IT#?-šđžčć_?", "full_path") == "This.is-how_we-do-IT"
